<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3>Navega</h3>
                <a href="home">Inicio</a>
            </div>
            <div class="col-md-3">
                <h3>Seccions</h3>
            <?php
                wp_nav_menu( array(
                    'theme_location'  => 'examen_wp_footer_interno',
                ) );
            ?>
            </div>
            <div class="col-md-3">
                <h3>Menú grups</h3>
            <?php
                wp_nav_menu( array(
                    'theme_location'  => 'examen_wp_footer_externo',
                ) );
            ?>
            </div>
        </div>
    </div>
</footer>

<?php wp_footer();?>
</body>

</html>