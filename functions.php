<?php
/* cargamos librerias de terceros */
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';
function examen_wp_config(){

    register_nav_menus(
        array(
            'examen_wp_main_menu' => 'examen_wp menu principal',
            'examen_wp_footer_interno' => 'examen_wp menu footer interno',
            'examen_wp_footer_externo' => 'examen_wp menu footer externo',
        )
    );

}
add_action('after_setup_theme', 'examen_wp_config', 0);

function wptest_scripts(){
    wp_enqueue_script("bootstrap_js", get_theme_file_uri("inc/js/bootstrap.min.js"), array("jquery"), "4.6", true );
    wp_enqueue_style("bootstrap_css", get_theme_file_uri("inc/css/bootstrap.min.css"), array(), "4.6", "all" );
    wp_enqueue_style("style_css", get_theme_file_uri("style.css"), array(), "1.0", "all" );
}
add_action('wp_enqueue_scripts', 'wptest_scripts');