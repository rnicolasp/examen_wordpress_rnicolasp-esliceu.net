<?php
get_header();
?>
<div class="jumbotron jumbo-tienda">
    <h1>Tenda</h1>
    <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
</div>

<div class="container">
    <div class="row">
    <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte01.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte02.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte03.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte04.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte05.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte06.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte07.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        <div class="card">
                <img src="<?= get_theme_file_uri("inc/img/producte08.jpg") ?>" alt="" srcset="">
                <div class="card-body">
                    <h5>Producte</h5>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <p>19,90€ <del>25,50€ </del></p>
                    <a href="" class="btn btn-primary"><i class="fas fa-cart-plus"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>