<?php
get_header();
?>
<div class="jumbotron jumbo-front">
    <h1>Mercadet es Liceu</h1>
    <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit.</p>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card">
                <img src="<?=get_theme_file_uri("inc/img/jumbo-tenda.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body izq">
                    <h5 class="card-title">Botiga</h5>
                    <p class="card-text">Entrar a la nostra botiga.</p>
                    <a href="<?=site_url("tenda")?>" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <img src="<?=get_theme_file_uri("inc/img/jumbo-novetats.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body mid">
                    <h5 class="card-title">Novetats</h5>
                    <p class="card-text">Darreres notícies.</p>
                    <a href="<?=site_url("tienda")?>" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card der">
                <img src="<?=get_theme_file_uri("inc/img/jumbo-about.jpg")?>" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Qui som</h5>
                    <p class="card-text">Qui forma el nostre equip.</p>
                    <a href="<?=site_url("ofertas")?>" class="btn btn-primary"><i class="fas fa-shopping-cart"></i></a>
                </div>
            </div>
        </div>
</div>
</div>
<?php
get_footer();
?>